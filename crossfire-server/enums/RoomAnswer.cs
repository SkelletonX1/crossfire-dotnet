﻿namespace crossfire_server.enums
{
    public enum RoomAnswer : byte
    {
        Success,
        Full,
        Unknown,
        WrongPassword
    }
}