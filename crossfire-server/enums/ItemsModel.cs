﻿namespace crossfire_server.enums
{
    public enum ItemsModel
    {
        Weapon,//C
        CharacterEquipment,//B
        Character,//A
        Item,//I
        Bag//D
    }
}