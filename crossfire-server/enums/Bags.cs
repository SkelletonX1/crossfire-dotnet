﻿namespace crossfire_server.enums
{
    public enum Bags : ushort
    {
        Bag1 = 1,
        Bag2,
        Bag3,
        Bag4,
        Bag5,
        Bag6,
        Bag7
    }
}