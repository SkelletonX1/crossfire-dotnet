﻿namespace crossfire_server.enums
{
    public enum WeaponTypes : byte
    {
        Shotgun = 0,
        Smg = 1,
        Rifle = 2,
        SniperRifle = 3,
        Mg = 4,
        Pistol = 5,
        Knife = 6,
        Grenade = 7
    }
}