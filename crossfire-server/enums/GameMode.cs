﻿namespace crossfire_server.enums
{
    public enum GameMode : byte
    {
        SearchAndDestroy = 1,
        TeamDeathMatch,
        ShadowMode,
        GhostMode,
        Elimination,
        FreeForAll,
        WeaponMaster = 36,
        EscapeMode,
        Wave,
        SuperSoldierTd,
        SuperSoldierSandD,
        SuppressionMode,
        SpyMode,
        CaptainMode,
        BombingMode,
        TeamEscapeMode
    }
}