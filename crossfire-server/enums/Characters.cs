﻿namespace crossfire_server.enums
{
    public enum Characters : ushort
    {
        Swat = 1,
        Omoh,
        Sas,
        Ulp,
        Hero,
        FoxHowl,
        GhostXf,
        GhostXm,
        Mos,
        UlpX,
        HumanBoss,
        Gsg9,
        GhostX,
        Ranger
    }
}