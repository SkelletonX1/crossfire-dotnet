﻿namespace crossfire_server.enums
{
    public enum ServerType : ushort
    {
        Normal = 1,
        Clan
    }
}