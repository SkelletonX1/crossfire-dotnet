﻿namespace crossfire_server.enums
{
    public enum RoomWeapons : byte
    {
        AllWeapons,
        Knife,
        Pistol,
        Sniper
    }
}