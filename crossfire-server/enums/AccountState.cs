﻿namespace crossfire_server.enums
{
    public enum AccountState : byte
    {
        Default = 0,
        Banned = 1,
        GameMaster = 100,
    }
}