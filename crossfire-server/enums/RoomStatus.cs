﻿namespace crossfire_server.enums
{
    public enum RoomStatus : byte
    {
        InGameCantJoin,
        Waiting,
        InGame,
    }
}