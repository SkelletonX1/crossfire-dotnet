﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crossfire_server.util.log.Enums
{
    public enum LogType
    {
        Information,
        Success,
        Warning,
        Error,
        Fatal
    }
}
